<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Excel;

class ApiExcelImportController 
{
    public function import(Request $request)
    {
  		// key file import
  		$path = $request->file('file')->getRealPath();
    	$data = \Excel::toArray('', $path, null, \Maatwebsite\Excel\Excel::TSV)[0];
    	$temp_array=[];
    	// excel import data arrange with valid key
    	for ($i=1; $i <count($data) ; $i++) { 
    		$obj=['Start Date'=>$data[$i][0],'End Date'=>$data[$i][1],'First Name'=>$data[$i][2],'Last Name'=>$data[$i][3],'Email'=>$data[$i][4],'Tel Number'=>$data[$i][5],'Address1'=>$data[$i][6],'Address2'=>$data[$i][7],'City'=>$data[$i][8],'Country'=>$data[$i][9],'Postcode'=>$data[$i][10],'Product Name'=>$data[$i][11],'Cost'=>$data[$i][12],'Currency'=>$data[$i][13],'Transaction Date'=>$data[$i][14]];
    		//every row insert into array with proper valid key
    		array_push($temp_array,$obj);
    	}
        // return data as json with paramter status , message and data
    	return response()->json(['status' => true, 'data' => $temp_array, 'errormessage' => [], 'successmessage' => "success"]);
    }
}
